# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, os, options, parseopt, streams, uri]
import eris, eris/url_stores

proc usage =
  stderr.writeLine """
Usage: erisput URL +FILE
Put data to an ERIS store over the CoAP or HTTP protocol.

Option flags:
	--1k        	 1KiB block size
	--32k       	32KiB block size (default for put from stdin)
	--convergent	Generate convergent URNs (unique by default)

"""
  quit QuitFailure

proc die(args: varargs[string, `$`]) =
  writeLine(stderr, args)
  if not defined(release): raiseAssert "die"
  quit QuitFailure

proc put(store: ErisStore; arg: string; bs: Option[BlockSize]; convergent: bool) =
  var
    stream: Stream
    bs = bs
  if arg == "-" or arg == "":
    if bs.isNone: bs = some bs32k
    stream = newFileStream(stdin)
  else:
    if not fileExists(arg):
      die arg, " does not exist as a file"
    if bs.isNone:
      if arg.getFileSize < (16.BiggestInt shl 10): bs = some bs1k
      else: bs = some bs32k
    stream = openFileStream(arg)
  var cap = waitFor encode(store, bs.get, stream, convergent)
  stdout.writeLine cap
  close stream

proc failParam(kind: CmdLineKind, key, val: string) =
  die "invalid parameter ", kind, " \"", key, ":", val, "\""

proc main*(opts: var OptParser) =
  var
    store: ErisStore
    args: seq[string]
    blockSize: Option[BlockSize]
    convergent: bool

  for kind, key, val in getopt(opts):
    case kind
    of cmdLongOption:
      if val != "": failParam(kind, key, val)
      case key
      of "1k": blockSize = some bs1k
      of "32k": blockSize = some bs32k
      of "convergent": convergent = true
      of "help": usage()
      else: failParam(kind, key, val)
    of cmdShortOption:
      case key
      of "h": usage()
      else: failParam(kind, key, val)
    of cmdArgument:
      if store.isNil:
        try:
          var url = parseUri(key)
          store = waitFor newStoreClient(url)
        except:
          die "failed to connect to ", key
      else:
        args.add key
    of cmdEnd: discard
  if store.isNil: die "no store URL specified"
  if args.len == 0: args.add "-"
  for arg in args: put(store, arg, blockSize, convergent)

when isMainModule:
  var opts = initOptParser()
  main opts
