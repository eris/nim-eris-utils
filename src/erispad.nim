# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[options, os, parseopt]
import eris

proc usage =
  stderr.writeLine """
Usage: eriscat FILE [FILE …]

Concatenate files to a stream with padding between ERIS block boundaries.
If the average file size is less than 16KiB then the output stream is padded to
align to 1KiB blocks, otherwise 32KiB.

This utility is intending for joining files in formats that support
concatenation such as Ogg containers. The resulting stream can be mostly
deduplicated with the individual encodings of each file.
"""

proc main*(opts: var OptParser) =
  var
    filePaths: seq[string]
    blockSize: Option[BlockSize]


  proc failParam(kind: CmdLineKind, key, val: string) =
    stderr.writeLine("unhandled parameter ", key, " ", val)
    quit 1

  for kind, key, val in getopt(opts):
    if val != "": failParam(kind, key, val)
    case kind
    of cmdLongOption:
      case key
      of "1k": blockSize = some bs1k
      of "32k": blockSize = some bs32k
      of "help":
        usage()
        quit QuitFailure
      else: failParam(kind, key, val)
    of cmdShortOption:
      case key
      of "h":
        usage()
        quit QuitFailure
      else: failParam(kind, key, val)
    of cmdArgument: filePaths.add(key)
    of cmdEnd: discard

  if filePaths == @[]:
    usage()
    quit("no files specified")

  for filePath in filePaths:
    if not (fileExists filePath):
      quit("not a file " & filePath)

  if blockSize.isNone:
    var totalSize: int
    for filePath in filePaths:
      let size = getFileSize(filePath)
      if size > 0:
        inc(totalSize, int size)
    blockSize = some recommendedBlockSize(totalSize div filePaths.len)

  var
    blkLen = blockSize.get.int
    blk = alloc(blkLen) # leak like a pro

  for i, path in filePaths:
    var f: File
    if not open(f, path): quit("failed to open " & path)
    while true:
      var n = readBuffer(f, blk, blkLen)
      if writeBuffer(stdout, blk, n) != n:
        quit("write error")
      if n != blkLen:
        if i < filePaths.high:
          let padLen = blkLen - n
          zeroMem(blk, padLen)
          cast[ptr byte](blk)[] = 0x80
          if writeBuffer(stdout, blk, padLen) != padLen:
            quit("write error")
        break
    close(f)

when isMainModule:
  var opts = initOptParser()
  main opts
