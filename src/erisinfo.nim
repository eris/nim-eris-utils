# SPDX-FileCopyrightText: 2022 Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/parseopt
from std/math import `^`
from std/strutils import formatSize
import eris

proc usage =
  stderr.writeLine """
Usage: erisinfo URN [URN…]

Get information on ERIS URNs.
"""
  quit QuitFailure


proc main*(opts: var OptParser) =
  var
    urns: seq[string]
    humanReadable = false

  for kind, key, val in getopt(opts):
    case kind
    of cmdLongOption, cmdShortOption:
      case key
      of "human-readable", "h": humanReadable = true
      else:
        stderr.writeLine "unhandled option flag ", key
        usage()
    of cmdArgument:
      urns.add key
    else:
      discard

  if urns.len == 0: usage()

  proc printInfo(label, s: string) =
    stdout.writeLine(label, s)

  proc printInfo(label: string; n: uint8) =
    stdout.writeLine(label, n)

  proc printInfo(label: string; n: Natural) =
    if humanReadable:
      stdout.writeLine(label, formatSize(n))
    else:
      stdout.writeLine(label, n)

  for urn in urns:
    try:
      let cap = parseErisUrn(urn)
      printInfo "       URN: ", $cap
      printInfo "block-size: ", cap.blockSize.int
      printInfo "     level: ", cap.level
      # TODO: minimum size
      printInfo "  max-size: ", pred((cap.blockSize.arity ^ cap.level.int) * cap.blockSize.int)
    except: discard

when isMainModule:
  var opts = initOptParser()
  main opts
